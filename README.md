# Fuchsia

Harness for SPEC CPU2000 benchmarks. To compile and build it, sources from SPEC
CPU2000 tarball needs to be unpacked in this folder.

To prepare the build, download the tar for SPEC\_CPU2000v1.3.tar and then run

```
./patch.sh path/to/SPEC\_CPU2000v1.3
```
This command will extract the tar, copy sources and patch them for fuchsia.
