// Copyright 2016 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#pragma once

#include <string>
#include <sys/time.h>

// Deletes Directory recursively.
// Retunrs errno if error in deleting
int DeleteDir(std::string dirname);

// Copies directory recursively.
int CopyDir(std::string dest_dirname, std::string src_dirname);

// Copies content of the file.
int CopyFile(std::string dest, std::string src);
