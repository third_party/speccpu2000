// Copyright 2016 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include <benchmark/benchmark.h>
#include <fstream>
#include <gflags/gflags.h>
#include <glog/logging.h>
#include <libgen.h>
#include <spec_fixture.h>
#include <spec_utils.h>
#include <sys/stat.h>
#include <unistd.h>

namespace {

class MesaFixture : public SpecFixture {
 protected:
  MesaFixture() : SpecFixture("177.mesa") {}
};

class GzipFixture : public SpecFixture {
 protected:
  GzipFixture() : SpecFixture("164.gzip"){};
};

class ArtFixture : public SpecFixture {
 protected:
  ArtFixture() : SpecFixture("179.art"){};
};

class EquakeFixture : public SpecFixture {
 protected:
  EquakeFixture() : SpecFixture("183.equake"){};
};

class McfFixture : public SpecFixture {
 protected:
  McfFixture() : SpecFixture("181.mcf"){};
};

class VortexFixture : public SpecFixture {
 protected:
  VortexFixture() : SpecFixture("255.vortex"){};
};

class Bzip2Fixture : public SpecFixture {
 protected:
  Bzip2Fixture() : SpecFixture("256.bzip2"){};
};

class TwolfFixture : public SpecFixture {
 protected:
  TwolfFixture() : SpecFixture("300.twolf"){};
};

class AmmpFixture : public SpecFixture {
 protected:
  AmmpFixture() : SpecFixture("188.ammp"){};
};

class GccFixture : public SpecFixture {
 protected:
  GccFixture() : SpecFixture("176.gcc"){};
};

class EonFixture : public SpecFixture {
 protected:
  EonFixture() : SpecFixture("252.eon"){};
};

class VprFixture : public SpecFixture {
 protected:
  VprFixture() : SpecFixture("175.vpr"){};
};

}  // namespace

std::string executableDir;

// Where we will run benchmarks
std::string tmpDir;

BENCHMARK_F(MesaFixture, 177_Mesa)(benchmark::State& st) {
  while (st.KeepRunning()) {
    if (RunSpec(NULL, 0, NULL) != 0) {
      st.SkipWithError(
          (std::string("Error while running benchmark: ") + strerror(errno))
              .c_str());
    }
  }
}

BENCHMARK_F(GzipFixture, 164_Gzip)(benchmark::State& st) {
  while (st.KeepRunning()) {
    std::vector<std::string> inputs = {"input.source", "input.log",
                                       "input.graphic", "input.random",
                                       "input.program"};

    for (auto& it : inputs) {
      const char* args[] = {it.c_str(), "60"};
      if (RunSpec(args, 2, NULL) != 0) {
        st.SkipWithError(
            (std::string("Error while running benchmark: ") + strerror(errno))
                .c_str());
      }
    }
  }
}

BENCHMARK_F(ArtFixture, 179_Art)(benchmark::State& st) {
  while (st.KeepRunning()) {
    const char* args1[] = {
        "-scanfile", "c756hel.in", "-trainfile1", "a10.img", "-trainfile2",
        "hc.img",    "-stride",    "2",           "-startx", "110",
        "-starty",   "200",        "-endx",       "160",     "-endy",
        "240",       "-objects",   "10"};

    const char* args2[] = {
        "-scanfile", "c756hel.in", "-trainfile1", "a10.img", "-trainfile2",
        "hc.img",    "-stride",    "2",           "-startx", "470",
        "-starty",   "140",        "-endx",       "520",     "-endy",
        "180",       "-objects",   "10"};
    if (RunSpec(args1, 18, NULL) != 0 || RunSpec(args2, 18, NULL) != 0) {
      st.SkipWithError(
          (std::string("Error while running benchmark: ") + strerror(errno))
              .c_str());
    }
  }
}

BENCHMARK_F(EquakeFixture, 183_equake)(benchmark::State& st) {
  while (st.KeepRunning()) {
    if ((RunSpec(NULL, 0, "inp.in")) != 0) {
      st.SkipWithError(
          (std::string("Error while running benchmark: ") + strerror(errno))
              .c_str());
    }
  }
}

BENCHMARK_F(McfFixture, 181_Mcf)(benchmark::State& st) {
  while (st.KeepRunning()) {
    const char* args[] = {"inp.in"};
    if (RunSpec(args, 1, NULL) != 0) {
      st.SkipWithError(
          (std::string("Error while running benchmark: ") + strerror(errno))
              .c_str());
    }
  }
}

BENCHMARK_F(VortexFixture, 255_Votex)(benchmark::State& st) {
  while (st.KeepRunning()) {
    for (int i = 1; i <= 3; i++) {
      std::string arg = "lendian" + std::to_string(i) + ".raw";
      const char* args[] = {arg.c_str()};
      if ((RunSpec(args, 1, NULL)) != 0) {
        (std::string("Error while running benchmark: ") + strerror(errno))
            .c_str();
      }
    }
  }
}

BENCHMARK_F(Bzip2Fixture, 256_Bzip2)(benchmark::State& st) {
  while (st.KeepRunning()) {
    const char* inputs[] = {"input.source", "input.program", "input.graphic"};
    for (int i = 0; i < 3; i++) {
      const char* args[] = {inputs[i], "58"};
      if ((RunSpec(args, 2, NULL)) != 0) {
        (std::string("Error while running benchmark: ") + strerror(errno))
            .c_str();
      }
    }
  }
}

BENCHMARK_F(TwolfFixture, 300_Twolf)(benchmark::State& st) {
  while (st.KeepRunning()) {
    const char* args[] = {"ref"};
    if ((RunSpec(args, 1, NULL)) != 0) {
      (std::string("Error while running benchmark: ") + strerror(errno))
          .c_str();
    }
  }
}

BENCHMARK_F(AmmpFixture, 188_Ammp)(benchmark::State& st) {
  while (st.KeepRunning()) {
    if ((RunSpec(NULL, 0, "ammp.in")) != 0) {
      (std::string("Error while running benchmark: ") + strerror(errno))
          .c_str();
    }
  }
}

// TODO(anmittal): Currently it doesn't work with fuchsia as getrlimit is not
// implemented. Re-enable this once that is implemented
#ifndef __Fuchsia__
BENCHMARK_F(GccFixture, 176_gcc)(benchmark::State& st) {
  const char* file_prefixes[] = {"166", "200", "expr", "integrate", "scilab"};
  while (st.KeepRunning()) {
    for (int i = 0; i < 5; i++) {
      const char* args[] = {(std::string(file_prefixes[i]) + ".i").c_str(),
                            "-o",
                            (std::string(file_prefixes[i]) + ".s").c_str()};
      if ((RunSpec(args, 3, NULL)) != 0) {
        (std::string("Error while running benchmark: ") + strerror(errno))
            .c_str();
      }
    }
  }
}
#endif

// TODO(anmittal): Re-enable this when musl implements times function
#ifndef __Fuchsia__
BENCHMARK_F(EonFixture, 252_Eon)(benchmark::State& st) {
  while (st.KeepRunning()) {
    const char* args1[] = {
        "chair.control.kajiya", "chair.camera", "chair.surfaces",
        "chair.kajiya.ppm",     "ppm",          "pixels_out.kajiya"};
    const char* args2[] = {
        "chair.control.cook", "chair.camera", "chair.surfaces",
        "chair.cook.ppm",     "ppm",          "pixels_out.cook"};
    const char* args3[] = {
        "chair.control.rushmeier", "chair.camera", "chair.surfaces",
        "chair.rushmeier.ppm",     "ppm",          "pixels_out.rushmeier"};
    if ((RunSpec(args1, 6, NULL)) != 0 || (RunSpec(args2, 6, NULL)) != 0 ||
        (RunSpec(args3, 6, NULL)) != 0) {
      st.SkipWithError(
          (std::string("Error while running benchmark: ") + strerror(errno))
              .c_str());
    }
  }
}
#endif

BENCHMARK_F(VprFixture, 175_Vpr)(benchmark::State& st) {
  const char* args1[] = {
      "net.in",      "arch.in", "place.out",  "dum.out", "-nodisp",
      "-place_only", "-init_t", "5",          "-exit_t", "0.005",
      "-alpha_t",    "0.9412",  "-inner_num", "2"};

  // clang-format off
  const char* args2[] = {
      "net.in", "arch.in", "place.in", "route.out", "-nodisp", "-route_only",
      "-route_chan_width", "15", "-pres_fac_mult", "2", "-acc_fac", "1",
      "-first_iter_pres_fac", "4", "-initial_pres_fac", "8"};
  // clang-format on

  while (st.KeepRunning()) {
    if ((RunSpec(args1, 14, NULL)) != 0 || (RunSpec(args2, 16, NULL)) != 0) {
      (std::string("Error while running benchmark: ") + strerror(errno))
          .c_str();
    }
  }
}

int main(int argc, char* argv[]) {
  ::benchmark::Initialize(&argc, argv);
  google::InitGoogleLogging(argv[0]);
  gflags::ParseCommandLineFlags(&argc, &argv, true);
  char* cwd;
  CHECK_NOTNULL((cwd = get_current_dir_name()));
  executableDir = std::string(cwd) + "/" + dirname(argv[0]);

  char* c = tempnam(NULL, "speccpu2000");
  CHECK_NOTNULL(c);
  tmpDir = c;

  CHECK_EQ(mkdir(tmpDir.c_str(), 0700), 0)
      << "Error while creating tmp dir: " << tmpDir.c_str()
      << ", error:" << strerror(errno);

  ::benchmark::RunSpecifiedBenchmarks();

  // try deleting temp directory
  CHECK_EQ(DeleteDir(tmpDir), 0) << "Error while deleting tmp dir: " << tmpDir
                                 << ", error:" << strerror(errno);
  return 0;
}
