// Copyright 2016 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#pragma once

#include <benchmark/benchmark.h>
#include <string.h>

class SpecFixture : public benchmark::Fixture {
 private:
  std::string run_dir_name;

  // Launch process defined by arg. It returns 1 if execution failure else
  // returns 0.
  int LaunchProcess(char** args, int args_length, const char* input_filename);

 protected:
  std::string benchmark_name;

  SpecFixture(std::string name) : benchmark_name(name){};

  void SetUp(benchmark::State& st) override;

  void TearDown(benchmark::State& st) override;

  int RunSpec(const char* args[], int args_length, const char* input_filename);
};
