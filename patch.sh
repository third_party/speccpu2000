#!/usr/bin/env bash
# Copyright 2016 The Fuchsia Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

readonly SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
readonly CINT_BLIST=(
  "164.gzip"
  "175.vpr"
  "176.gcc"
  "181.mcf"
  "186.crafty"
  "197.parser"
  "252.eon"
  "253.perlbmk"
  "254.gap"
  "255.vortex"
  "256.bzip2"
  "300.twolf"
)
readonly CFP_BLIST=(
  "168.wupwise"
  "171.swim"
  "172.mgrid"
  "173.applu"
  "177.mesa"
  "178.galgel"
  "179.art"
  "183.equake"
  "187.facerec"
  "188.ammp"
  "189.lucas"
  "191.fma3d"
  "200.sixtrack"
  "301.apsi"
)

function copynpatch() {
  local benchmark_dir=$1
  shift
  for i in $@; do
    local benchmark="${benchmark_dir}/${i}"
    if [[ -d "${benchmark}" ]]; then
      # check if we support that benchmark
      if [[ -e "${SCRIPT_DIR}/${i}" ]]; then
        echo "Copying src for ${i}....."
        cp -r  -- "${benchmark}/src" "${SCRIPT_DIR}/${i}/"
        # Some src have only read permission, so if script is
        # executed again source are not replaced
        chmod -R u+w -- "${SCRIPT_DIR}/${i}/src"

        echo "Copying data for ${i}....."
        cp -r -- "${benchmark}/data" "${SCRIPT_DIR}/${i}/"
        chmod -R u+w -- "${SCRIPT_DIR}/${i}/data"

        # check if we have a diff
        if [[ -e "${SCRIPT_DIR}/${i}/fuchsia.patch" ]] ; then
          echo "Patching ${i}.........."
          patch -d "${SCRIPT_DIR}/${i}" --verbose -p0 < "${SCRIPT_DIR}/${i}/fuchsia.patch"
        fi
        echo
      fi
    fi
  done
}

if [[ $# -ne 1 ]]; then
  echo "usage: $0 <path/to/SPEC_CPU2000v1.3>"
  exit 1
fi

copynpatch "$1/benchspec/CINT2000" ${CINT_BLIST[*]}
copynpatch "$1/benchspec/CFP2000" ${CFP_BLIST[*]}

mv "${SCRIPT_DIR}/177.mesa/data/ref/input/mesa.in" "${SCRIPT_DIR}/177.mesa/data/ref/input/mesa.mesh"
touch "${SCRIPT_DIR}/181.mcf/data/ref/input/mcf.out"
